package service;

import java.util.Arrays;

/**
 * The utils class that helps manipulate data.
 */
public class DataUtils {
    /**
     *
     * @param input string with no more than 10 numbers split by space.
     * @return array with ints from provided string.
     */
    public static int[] getIntArrayFromString(String input) {
        if (input == null || input.isEmpty()) {
            throw new IllegalArgumentException("Please provide data for sorting");
        }
        String[] strArr = input.split("\\s");
        if(strArr.length > 10) {
            throw new IllegalArgumentException("Please provide no more that 10 numbers");
        }
        int[] ints = new int[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            ints[i] = Integer.parseInt(strArr[i]);
        }
        return ints;
    }

    /**
     * @param arr array with ints that should be sorted.
     * @return sorted array.
     */
    public static int[] sortIntArrayAsc(int[] arr) {
        Arrays.sort(arr);
        return arr;
    }

    /**
     *
     * @param arr array that should be printed.
     */
    public static void printIntsArray(int[] arr) {
        System.out.println(Arrays.toString(arr));
    }
}
