package controller;

import service.DataUtils;

import java.util.Scanner;

/**
 * The class that interacts with user.
 */
public class DataController {

    /**
     * @param args should be an one string with numbers(split by space)
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String intsString = null;
        if (scanner.hasNextLine()) {
            intsString = scanner.nextLine();
        }
        int[] ints = DataUtils.getIntArrayFromString(intsString);
        DataUtils.printIntsArray(DataUtils.sortIntArrayAsc(ints));
    }
}
