package service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class DataUtilsSortingTest {

    private int[] input;
    private int[] expected;

    public DataUtilsSortingTest(int[] input, int[] expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{1}, new int[]{1}},
                {new int[]{3, 5, 1}, new int[]{1, 3, 5}},
                {new int[]{1, 4, 5}, new int[]{1, 4, 5}},
                {new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0}, new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}}
        });
    }

    @Test
    public void regularStringToIntsArrConverting() {
        Assert.assertArrayEquals(expected, DataUtils.sortIntArrayAsc(input));
    }
}
