package service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class DataUtilsConvertEmptyTest {
    private String input;

    public DataUtilsConvertEmptyTest(String input) {
        this.input = input;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {""},
                {null}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void regularStringToIntsArrConverting() {
        DataUtils.getIntArrayFromString(input);
    }
}
