package service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class DataUtilsConvertMoreThanTenTest {
    private String input;

    public DataUtilsConvertMoreThanTenTest(String input) {
        this.input = input;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"0 1 2 3 4 5 6 7 8 9 10"},
                {"1 1 1 1 1 1 1 1 1 1 1 1 1 "}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void regularStringToIntsArrConverting() {
        DataUtils.getIntArrayFromString(input);
    }
}
