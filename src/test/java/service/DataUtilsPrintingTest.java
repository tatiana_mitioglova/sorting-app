package service;

import org.junit.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class DataUtilsPrintingTest {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    public void givenSystemOutRedirection_whenInvokePrintln_thenOutputCaptorSuccess() {
        DataUtils.printIntsArray(new int[]{1, 2});

        Assert.assertEquals("[1, 2]", outputStreamCaptor.toString().trim());
    }

    @After
    public void tearDown() {
        System.setOut(standardOut);
    }
}
